// Vue
import { createComponent } from "@vue/composition-api";
// Hooks
import { useStore } from "@/hooks";
// Types
import {} from "@/types";
// CSS
import styles from "@/views/home/styles.module.scss";
// Components
import { Example } from "@/components";

interface HomeViewProps {}

const HomeView = createComponent<HomeViewProps, {}>({
  props: {},
  setup(props, context) {
    // const store = useStore();

    return () => {
      return (
        <div id="home" class={styles["home"]}>
          <Example />
        </div>
      );
    };
  }
});

export default HomeView;

export { HomeView };
