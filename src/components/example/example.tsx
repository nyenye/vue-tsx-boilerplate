// Vue
import { createComponent, computed, onMounted } from "@vue/composition-api";
// CSS
import styles from "@/components/example/styles.module.scss";

export interface ExampleProps {}

export const Example = createComponent<ExampleProps, {}>({
  props: {},
  setup(props: Readonly<ExampleProps>) {
    return () => <div class={styles["example"]}>Example Component</div>;
  }
});
