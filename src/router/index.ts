// Vue
import Vue from "vue";
// VueRouter
import VueRouter, { Route, RouteConfig } from "vue-router";
// Routes
import { ROUTE_HOME } from "@/router/routes";
// Components
import { HomeView } from "@/views";

Vue.use(VueRouter);

const routes = [
  {
    ...ROUTE_HOME,
    component: HomeView
    // props: {
    // hook: someHook
    // }
  }
  // {
  //   ...ROUTE_OTHER,
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => {
  //     return import(
  //       /* webpackChunkName: "newest" */ "../views/item-details/item-details"
  //     );
  //   },
  //   props: (route: Route) => {
  //     return { id: parseInt(route.params.id) };
  //   }
  // }
];

const router = new VueRouter({
  mode: "history",
  routes: routes as Array<RouteConfig>,
  scrollBehavior: () => ({ x: 0, y: 0 })
});

router.beforeEach((to: Route, from: Route, next: Function) => {
  next();
});

export default router;
