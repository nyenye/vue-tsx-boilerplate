// Vue
import Vue from "vue";
// Vuex
import Vuex, { Store } from "vuex";
// Vuex Plugins
import createLogger from "vuex/dist/logger";
import VuexPersistence from "vuex-persist";
// Types
import { RootState } from "@/types";
// Store Modules
import modules from "@/store/modules";

Vue.use(Vuex);

const vuexLocalStorage = new VuexPersistence<RootState>({});

const store = new Store<RootState>({
  strict: process.env.NODE_ENV !== "production",
  modules,
  plugins: [createLogger(), vuexLocalStorage.plugin]
});

export default store;
