// Vuex
import { ActionTree, ActionHandler, ActionContext } from "vuex";
// Types
import { ExampleState, RootState } from "@/types";

const actions: ActionTree<ExampleState, RootState> = {};

export default actions;
