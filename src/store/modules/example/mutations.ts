import { MutationTree, Mutation } from "vuex";
import { ExampleState } from "@/types";

const mutations: MutationTree<ExampleState> = {};

export default mutations;
