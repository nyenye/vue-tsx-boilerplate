import { Module } from "vuex";
import { ExampleState, RootState } from "@/types";

import state from "@/store/modules/example/state";
import getters from "@/store/modules/example/getters";
import actions from "@/store/modules/example/actions";
import mutations from "@/store/modules/example/mutations";

const module: Module<ExampleState, RootState> = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};

export default module;
