import { ModuleTree } from "vuex";

import { RootState } from "@/types";
import { NS_EXAMPLE } from "@/store/namespaces";
import ExampleModule from "@/store/modules/example";

const rootModules: ModuleTree<RootState> = {
  [NS_EXAMPLE]: ExampleModule
};

export default rootModules;
